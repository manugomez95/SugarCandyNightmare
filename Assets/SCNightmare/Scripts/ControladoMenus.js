﻿#pragma strict
public var HasDinamicMenu : boolean = false;
public var DinamicMenu : GameObject;
public var ShowingMenu : boolean = false;
private var mouseLook : UnityStandardAssets.Characters.FirstPerson.MouseLook;
private var senx : double = 0;
private var seny : double = 0;
function Start(){
	Time.timeScale = 1.0;
	if(HasDinamicMenu == false){
		Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
	}else{
		Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
		mouseLook = GameObject.Find("Player").GetComponent.<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().m_MouseLook;
        senx = mouseLook.XSensitivity;
        seny = mouseLook.YSensitivity;
	}
}

function Play(){
	SceneManagement.SceneManager.LoadScene("principal");
}

function Update(){
	if(Input.GetButtonDown ("Start") && HasDinamicMenu != false){
		Debug.Log("tru");
		ShowingMenu = !ShowingMenu;
		if(ShowingMenu == true){
			Time.timeScale = 0.0;
			DinamicMenu.SetActive(true);
			Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            mouseLook.XSensitivity = 0;
            mouseLook.YSensitivity = 0;
		}else{
			Resume();
		}
	}
}

function Resume(){
	if(HasDinamicMenu == false)
		return;
	Time.timeScale = 1.0;
	Cursor.lockState = CursorLockMode.Locked;
    Cursor.visible = false;
	DinamicMenu.SetActive(false);
	mouseLook.XSensitivity = senx;
	mouseLook.YSensitivity = seny;
	ShowingMenu = false;
}

function Menu(){
	SceneManagement.SceneManager.LoadScene("Menu");
}
function Exit () {
	Application.Quit();
}
