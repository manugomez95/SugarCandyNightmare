﻿#pragma strict
public var Speed : double = 0.25;
public var DeathButtons : GameObject;
public var DeathText : GameObject;
public var Player : GameObject;
public var Image : Image;
function Start () {
	
}

function Update () {
		Image.color.a = Mathf.Min(1.0, Image.color.a + Speed * Time.deltaTime) ;
		if(Image.color.a  >= 1)
			Active();
}

function Active(){

	Time.timeScale = 0;
	Cursor.lockState = CursorLockMode.None;
	Cursor.visible = true;
	Destroy(Player.GetComponent.<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>());
	DeathButtons.SetActive(true);
	DeathText.SetActive(true);
}
