﻿#pragma strict

//SPAWNS TAG = Spawn
//ENEMIES TAG = Enemy

public var Numero_Hordas : int = 10;
public var Hordas : HordaInformacion[];
public var Horda_Actual : int = 0;
public var EnemigosRestantes : double = 0;
public var EnemigosMax : double = 0;
public var EnemyBar : UI.Image;
public var EnemyText : UI.Text;
public class HordaInformacion {
	public var Enemigos : Enemigo[];
	public var Fija : boolean = true;
	public var Grupos : int = 2;
	public var Interval : double = 0.5f;
}
public class Enemigo{
    public var Enemigo_Objeto : GameObject;
    public var Numero : int = 1;
    public var Offset : int = 0;
    public var OffsetAdd : int = 0;
}

function Start () {

	Horda_Actual++;

	//InvokeRepeating("NuevaHorda",0,45);
	NuevaHorda();
	//InvokeRepeating("UpdateEnemigosRestantes",0,1);
}

function Update () {
	
}

function NuevaHorda() {
	while(true){
		Debug.Log("New horda");
		//Obtener puntos de aparacion de enemigos
		var spawns : GameObject[] = GameObject.FindGameObjectsWithTag("Spawn");

		var info : HordaInformacion = Hordas[Horda_Actual-1];
		for(var e : Enemigo in info.Enemigos){
			EnemigosRestantes += e.Numero;
		}
		EnemigosMax = EnemigosRestantes;
		var i = 0;
		for(var e : Enemigo in info.Enemigos){
			var en = e.Numero;
			while (e.Numero > 0){
				var spawn = spawns[Random.Range(0,spawns.Length)];
				while (i < info.Grupos){
					
					Instantiate(e.Enemigo_Objeto,spawn.transform.position+ Vector3(Random.Range(-4.0-e.Offset,4.0+e.Offset),0,Random.Range(-4.0-e.Offset,4.0+e.Offset))+Vector3.up * (Random.Range(-4.0-e.Offset,4.0+e.Offset) + e.OffsetAdd ),Quaternion.identity);
					e.Numero--;
					i++;
					if(e.Numero == 0)
						break;
				}
				if(i >= info.Grupos){
					yield WaitForSeconds(info.Interval);
					i = 0;
				}
			}
			e.Numero = en;
		}
		yield WaitForSeconds(45);
	}
}


function UpdateEnemigosRestantes() {
	//EnemigosRestantes = GameObject.FindGameObjectsWithTag("Enemy").Length;
	//EnemyBar.rectTransform.sizeDelta.x = EnemigosRestantes / EnemigosMax * 300;
	//EnemyText.text = EnemigosRestantes+"";
}