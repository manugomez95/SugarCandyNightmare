﻿#pragma strict

class BallClass {
	public var Size : double;
	public var Health : int;
	public var Damage : double;
	public var BallColor : Color;
	public var Speed : double;
}

private var Agent : UnityEngine.AI.NavMeshAgent;
private var Target : GameObject;
private var Rigid : Rigidbody;
private var Body : GameObject;
public var Explosion : GameObject;
public var ExplosionSound : AudioClip;
public var ExplosionRadius : double = 3.0;
public var JumpSound : AudioClip;
private var Activated : boolean = false;
private var ActivatedTime : float = 0f;
public var JumpCooldown : float = 4.0f;

public var MinSize : double = 1;
public var MaxSize : double = 2.5;
public var Damage : double = 1;
public var BallClases : BallClass[];

function Start () {
	Agent = gameObject.GetComponent.<UnityEngine.AI.NavMeshAgent>();
	Target = GameObject.FindGameObjectWithTag("Player");
	Body = gameObject.transform.Find("Body").gameObject;
	Rigid = Body.GetComponent.<Rigidbody>();
	Randomize();
}

function Update () {
	Agent.SetDestination(Target.transform.position);
	Body.transform.Rotate (Vector3.right *  520 * Time.deltaTime);
	var distance = Vector3.Distance(transform.position, Target.transform.position);
	if(distance < 1.5){
		Explote();
	}else if(distance < 7.0){
		if(!Activated)
			Jump();
	}
	if(Activated){
		ActivatedTime+= Time.deltaTime;
		if(ActivatedTime >= JumpCooldown){
			Activated = false;
			ActivatedTime = 0;
		}
	}
		
		
}

function FixedUpdate(){
	if(!Physics.Raycast(Body.transform.position,Vector3.down,0.25)){
		Rigid.AddForce(Vector3.down * 9.8);
	}
}
function Jump(){
	Rigid.AddForce(Vector3.up * 400);
	AudioSource.PlayClipAtPoint(JumpSound, Body.transform.position);
	Activated = true;
}

function Explote() {
	var explosion : GameObject = Instantiate(Explosion,Body.transform.position,Quaternion.identity);
	AudioSource.PlayClipAtPoint(ExplosionSound, explosion.transform.position);
	Destroy(explosion,0.5);
	Destroy(gameObject);
	GameObject.Find("GameController").GetComponent.<ControladorHordas>().EnemigosRestantes -= 1;
	GameObject.Find("GameController").GetComponent.<ControladorHordas>().UpdateEnemigosRestantes();
	var pl = GameObject.Find("Player");
	if(Vector3.Distance(transform.position,pl.transform.position) < ExplosionRadius)
		pl.SendMessage("TakeDamage",Damage);
}

function OnDie(){
	Explote();
}

function Randomize(){
	var index = Random.Range(0,BallClases.length);
	var clase : BallClass = BallClases[index];

	var Dulce : GameObject = transform.Find("Body").transform.Find("Dulce").gameObject;
	var Envoltorio : GameObject = transform.Find("Body").transform.Find("Envoltorio").gameObject;
	Dulce.GetComponent.<Renderer>().materials[0].color = clase.BallColor;
	Envoltorio.GetComponent.<Renderer>().materials[0].color = clase.BallColor;

	Agent.speed = clase.Speed;
	gameObject.transform.Find("Body").gameObject.GetComponent.<Health>().Health = clase.Health; 
	transform.localScale = Vector3.one * clase.Size;
	Damage = clase.Damage;
}