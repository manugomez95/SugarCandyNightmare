﻿public var Player : GameObject;
public var Delay : float = 0;
public var Range : float = 3;
public var Velocity : float = 30;
public var Torque : float = 0.5;
public var dif : Vector3;
public var alive : boolean = true;
public var Explosion : GameObject;
public var LastPosition : Vector3 = Vector3.zero;
public var Direction : Vector3 = Vector3.zero;
public var Missile : GameObject;
function Start () {
    Player = GameObject.FindWithTag("Player");
}

function Update () {
	
	if(alive){
	    var factor = 1;
	    if(Delay <= 0){
	        if(Vector3.Distance(new Vector3(gameObject.transform.position.x,0,gameObject.transform.position.z),new Vector3(Player.transform.position.x,0,Player.transform.position.z)) < Range){
	            Shoot();
	            Delay = 4 + Random.Range(0.0f,1.0f);
	        }
	    }else{
	        factor = 0.5 + Random.Range(-0.2f,0.2f);
	    }
	    dif = Player.transform.position - transform.position;
	    dif.y = 0;
	    var newDir = Vector3.RotateTowards(transform.forward, dif, Torque * factor * Time.deltaTime, 0.0F);
	    transform.rotation = Quaternion.LookRotation(newDir);
	    Delay -= Time.deltaTime;
		transform.Translate(Vector3.forward * Velocity * Time.deltaTime);
	}else{
		Direction = gameObject.GetComponent.<Rigidbody>().velocity ;
		//transform.LookAt(  gameObject.GetComponent.<Rigidbody>().velocity + Direction);
		transform.eulerAngles.x += 20 * Time.deltaTime;
	}
}
function Shoot(){
	yield WaitForSeconds(Random.Range(0.0,0.25));
	Instantiate(Missile,transform.position,Quaternion.identity);
}

function OnDie(){
	alive = false;
	Destroy(gameObject.GetComponent.<Health>());
	gameObject.tag = "Untagged";
	gameObject.AddComponent.<Rigidbody>();
	var r = gameObject.GetComponent.<Rigidbody>();
	r.AddRelativeForce(Vector3.forward * 2000);
	r.AddRelativeTorque(Vector3.forward * Random.Range(-1.0,1.0) * 450);
	Destroy(Instantiate(Explosion,transform.position-transform.forward * 10+ transform.up,Quaternion.identity),0.5f);
	GameObject.Find("GameController").GetComponent.<ControladorHordas>().EnemigosRestantes -= 1;
	GameObject.Find("GameController").GetComponent.<ControladorHordas>().UpdateEnemigosRestantes();
	//r.UseGravity = true;
}

function OnCollisionEnter( collision : Collision){
	if(!alive){
		Instantiate(Explosion,transform.position-transform.forward * 10+ transform.up,Quaternion.identity);
		Destroy(gameObject.GetComponent.<Rigidbody>());
		Destroy(gameObject.GetComponent.<Plane>());
	}
}