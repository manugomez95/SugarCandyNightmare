﻿#pragma strict
public var Explosion : GameObject;
public var ExplosionRadius : double = 4.0f;

function Start () {
	
}

function Update () {
	
}

function OnCollisionEnter(collision : Collision){
	Destroy(Instantiate(Explosion,transform.position,Quaternion.identity),0.33);
	var pl = GameObject.Find("Player");
	if(Vector3.Distance(transform.position,pl.transform.position) < ExplosionRadius)
		pl.SendMessage("TakeDamage",1);
	Destroy(gameObject);
}