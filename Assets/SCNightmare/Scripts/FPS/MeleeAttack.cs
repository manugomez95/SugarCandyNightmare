﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeAttack : MonoBehaviour {

	public Animator animator;
	private bool isAttacking = false;
	public float attackTime = 1f;
	public int damage = 1;
	private Collider weaponCollider;

	// Use this for initialization
	void Start () {
		
	}

	void OnEnable() {
		weaponCollider = GetComponent<Collider>();
		weaponCollider.enabled = false;
		isAttacking = false;
		animator.SetBool ("Attacking", false);
	}
	
	// Update is called once per frame
	void Update () {
		if (isAttacking) {
			weaponCollider.enabled = true;
			return;
		}
		else
			weaponCollider.enabled = false;
		if (Input.GetButtonDown ("Fire1") || Mathf.Round(Input.GetAxisRaw("Fire1")) < 0) {
			StartCoroutine (Attack ());
			return;
		}
	}

	private IEnumerator Attack() {
		isAttacking = true;
		animator.SetBool ("Attacking", true);
		yield return new WaitForSeconds (attackTime - .25f);
		//attackAudio.Play();
		animator.SetBool ("Attacking", false);
		yield return new WaitForSeconds (.25f);
		isAttacking = false;
	}

	void OnTriggerEnter(Collider other) {
		other.gameObject.SendMessage ("TakeDamage", damage);
		Debug.Log (other.gameObject.name);
	}
}
