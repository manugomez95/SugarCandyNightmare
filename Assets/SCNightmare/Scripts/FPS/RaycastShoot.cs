﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RaycastShoot : MonoBehaviour {
	public int gunDamage = 1;
	public float fireRate = .25f;
	public float weaponRange = 50f;
	public float hitForce = 100f;
	public Transform gunEnd;
	public bool dual = false;
	private Camera fpsCam;
	public GameObject pointer;
	public Text ammoText;
	private WaitForSeconds shotDuration = new WaitForSeconds(.07f);

	public int remainingRounds = 20;
	public int maxAmmo = 10;
	private int currentAmmo;
	public float reloadTime = 1f;
	private bool isReloading = false;
	public Animator animator;

	private float volLowRange = .5f;
	private float volHighRange = 1.0f;

	public AudioClip shootSound;
	public AudioClip hitMark; 
	public GameObject hitObject;
	private AudioSource gunAudio;
	private AudioSource reloadAudio;
	private AudioSource emptyGun;
	private Animation gunAnimation;
	private Animation gunAnimation1;
	private Animation gunAnimation2;
	private float nextFire;

	public GameObject ShootEffect;
	public GameObject ShootHitEffect;
	// Use this for initialization
	void Start () {
		currentAmmo = maxAmmo;
		ammoText.text = printAmmo ();
		AudioSource[] allMyAudioSources = GetComponents<AudioSource>();
		gunAudio = allMyAudioSources[0];
		reloadAudio = allMyAudioSources[1];
		emptyGun = allMyAudioSources[2];
		fpsCam = GetComponentInParent<Camera> ();
		InvokeRepeating("DetectEnemy", 0, 0.16f);
		if (dual) {
			gunAnimation1 = this.gameObject.transform.GetChild (0).GetComponent<Animation> ();
			gunAnimation2 = this.gameObject.transform.GetChild (1).GetComponent<Animation> ();
		} else {
			gunAnimation = GetComponent<Animation>();
		}
	}

	void OnEnable() {
		ammoText.text = printAmmo ();
		isReloading = false;
		animator.SetBool ("Reloading", false);
	}

	// Update is called once per frame
	void Update () {
		if (isReloading)
			return;
		if ((Input.GetButton ("Fire1") || Mathf.Round(Input.GetAxisRaw("Fire1")) < 0) && Time.time > nextFire && Time.timeScale > 0) {
			if (currentAmmo == 0) {
				if (!emptyGun.isPlaying)
					emptyGun.Play();
			} else {
				currentAmmo--;
				//Crear efecto disparo 
				Instantiate(ShootEffect,transform.position,Camera.main.transform.rotation);
				ammoText.text = printAmmo ();
				nextFire = Time.time + fireRate;
				StartCoroutine (ShotEffect ());
				Vector3 rayOrigin = fpsCam.ViewportToWorldPoint (new Vector3 (.5f, .5f, 0));
				RaycastHit hit;
				if (Physics.Raycast (rayOrigin, fpsCam.transform.forward, out hit, weaponRange)) {
					if (hit.collider.gameObject.tag == "Enemy") {
						hit.collider.gameObject.SendMessage ("TakeDamage", gunDamage);
						gunAudio.PlayOneShot (hitMark,0.75f);
						GameObject g = Instantiate (hitObject, Vector3.zero, Quaternion.identity);
						g.transform.SetParent(GameObject.Find ("Puntero").transform);
						g.GetComponent<RectTransform>().anchoredPosition = new Vector2(-25,0);
						Destroy (g,0.16f);
					}
					Destroy(Instantiate (ShootHitEffect, hit.point, Quaternion.identity),0.3f);
					if (hit.rigidbody != null) {
						//hit.rigidbody.AddForce (-hit.normal * hitForce);
					}
				}
			}
		}
		if (Input.GetButtonDown ("Reload")) {
			if (currentAmmo != maxAmmo && remainingRounds != 0) {
				StartCoroutine (Reload ());
				return;
			}
		}
	}

	private IEnumerator ShotEffect() {
		float vol = Random.Range (volLowRange, volHighRange);
		gunAudio.PlayOneShot(shootSound,vol);
		if (dual) {
			gunAnimation1.Play ("Shoot");
			gunAnimation2.Play ("Shoot");
		} else {
			gunAnimation.Play ("Shoot");
		}
		yield return shotDuration;
	}

	void DetectEnemy()
	{
		Vector3 rayOrigin = fpsCam.ViewportToWorldPoint (new Vector3 (.5f, .5f, 0));
		RaycastHit hit;
		if (Physics.Raycast (rayOrigin, fpsCam.transform.forward, out hit, weaponRange) && hit.transform.tag == "Enemy") {
			pointer.GetComponent<Image> ().color = new Color32 (255, 0, 0, 255);
		} else {
			pointer.GetComponent<Image> ().color = new Color32 (255, 255, 255, 255);
		}
	}

	private IEnumerator Reload() {
		isReloading = true;
		animator.SetBool ("Reloading", true);
		yield return new WaitForSeconds (reloadTime - .25f);
		if (remainingRounds == -1)
			currentAmmo = maxAmmo;
		else {
			if (remainingRounds < maxAmmo) {
				if (currentAmmo + remainingRounds < maxAmmo) {
					currentAmmo += remainingRounds;
					remainingRounds = 0;
				} else {
					remainingRounds = remainingRounds - (maxAmmo - currentAmmo);
					currentAmmo = maxAmmo;
				}
			} else {
				remainingRounds -= maxAmmo-currentAmmo;
				currentAmmo = maxAmmo;
			}
		}
		reloadAudio.Play();
		animator.SetBool ("Reloading", false);
		yield return new WaitForSeconds (.25f);
		ammoText.text = printAmmo ();
		isReloading = false;
	}

	private string printAmmo() {
		if (remainingRounds == -1)
			return ammoText.text = currentAmmo.ToString() + "/∞";
		else
			return currentAmmo.ToString() + "/" + remainingRounds.ToString();
	}
}
