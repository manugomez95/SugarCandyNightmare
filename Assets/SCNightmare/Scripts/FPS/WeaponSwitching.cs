﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponSwitching : MonoBehaviour {

	public GameObject ammoText;
	public int selectedWeapon = 0;

	// Use this for initialization
	void Start () {
		SelectWeapon ();
	}
	
	// Update is called once per frame
	void Update () {
		/*Mostrar municion o no*/
		if (transform.GetChild (selectedWeapon).tag == "Melee")
			ammoText.SetActive (false);
		else
			ammoText.SetActive (true);

		int previousSelectedWeapon = selectedWeapon;
		if (Input.GetAxis ("Mouse ScrollWheel") > 0f || Input.GetButtonDown("ChangeWeapon")) {
			if (selectedWeapon >= transform.childCount - 1)
				selectedWeapon = 0;
			else
				selectedWeapon++;
		}
		if (Input.GetAxis ("Mouse ScrollWheel") < 0f) {
			if (selectedWeapon <= 0)
				selectedWeapon = transform.childCount - 1;
			else
				selectedWeapon--;
		}
		/*Soporte para 3 armas*/
		if (Input.GetKeyDown (KeyCode.Alpha1))
			selectedWeapon = 0;
		if (Input.GetKeyDown (KeyCode.Alpha2) && transform.childCount >= 2)
			selectedWeapon = 1;
		if (Input.GetKeyDown (KeyCode.Alpha3) && transform.childCount >= 3)
			selectedWeapon = 2;

		if (previousSelectedWeapon != selectedWeapon)
			SelectWeapon ();
	}

	void SelectWeapon() {
		int i = 0;
		foreach (Transform weapon in transform) {
			if (i == selectedWeapon)
				weapon.gameObject.SetActive (true);
			else
				weapon.gameObject.SetActive (false);	
			i++;
		}
	}
}
