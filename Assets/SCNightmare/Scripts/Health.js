﻿#pragma strict
public var Health : int = 100;
public var SendMessageToParent : boolean = true;
function Start () {
	
}

function Update () {
	if(Health <= 0){
		ControladorPuntuacion.score += 1;
		if(SendMessageToParent){
			gameObject.transform.parent.gameObject.SendMessage("OnDie");
		}else{
			gameObject.SendMessage("OnDie");
		}
	} 	
}

function TakeDamage(damage : int){
	Health -= damage;
}