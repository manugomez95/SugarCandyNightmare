﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HealthPlayer : MonoBehaviour {
	public float MaxHealth = 2;
	public float Health = 2;
	public bool SendMessageToParent = true;
	private GameObject PlayerHealthUI;
	public GameObject DeathMenu;
	public AudioClip takeDamageSound;
	private AudioSource audio;
	// Use this for initialization
	void Start () {
		audio = GetComponent<AudioSource>();
		PlayerHealthUI = GameObject.Find ("PlayerHealthUI");
		UpdateHealthUI ();
	}

	// Update is called once per frame
	void Update () {
		if(Health <= 0){
			DeathMenu.SetActive (true);
		} 
	}

	void TakeDamage(int damage){
		audio.PlayOneShot (takeDamageSound, 1);
		Health -= damage;
		UpdateHealthUI ();
	}

	void UpdateHealthUI() {
		GameObject heart;
		int i,j;
		for (i=0; i<MaxHealth; i++) {
			heart = PlayerHealthUI.transform.GetChild (i).gameObject;
			for (j = 0; j < 2; j++) {
				if((i*2)+j+1 <= Health*2)
					heart.transform.GetChild (j).gameObject.SetActive (true);
				else
					heart.transform.GetChild (j).gameObject.SetActive (false);
			}
		}
	}
}
